Git Exercises
==================

Starting a feature branch
------------------------------

1. Double click on master branch

2. Make sure `master` is now bold

3. Start working on your own feature branch
   by clicking the Branch button, and type in
   the name of the feature you are working on.
   I suggest if you are working on ticket 122,
   you could name your branch like this
   `feature/122-plot-thickens`.

4. At this stage your feature branch should 
   be in bold.

5. Select `Working Copy`. It should either be empty
   or say "Nothing to Commit" as you have a 
   fresh copy of the repository.

6. There is a dropdown that shows "Pending
   files, sorted by file status". Change that 
   to "All". You should see all the files in your
   repository.

7. Open all the Chapters in notepad by right-clicking
   on each file and clicking Open `SHIFT+CTRL+O`.
   Alternatively you can "Show in Explorer" and then
   open the file using your own tool/software.

Making changes
------------------

1. Make one change in Chapter1.txt, one change
   in Chapter2.txt and two changes on separate
   paragraphs in Chapter3.txt. we are going to 
   pretend that you'd like to check-in Chapter1.txt,
   not check in Chapter2.txt at all, and you'd
   like to check in only one of two the changes
   you made in Chapter 3.

2. Change the drop down box from "All" back to
   "Pending files", as it makes it easier to
   work out what files have changed. You should
   see a red line for the line that will be replaced
   by the text on the green line. If you see
   the entire file has gone red, and there's
   there is a green section with the entire file in
   it, chances are you have run into a line ending
   problem. This will be have to be dealt in a later
   chapter.

3.  Add the entirety of Chapter1 to the staging area
    by selecting the check box. When you stage a file
    you haven't made a check in yet. 

4.  Go to chapter3 and select `Stage hunk` for one
    of the changes you made, and leave the other
    alone.

5.  Provide a commit message. If you have a Rally
    ticket number this should be included in the 
    commit message and then hit "Commit". Your 
    changes have now been committed to your personal
    copy of the source control system.

6.  You should push your changes to the original
    source control repository. This is known as the
    "origin". Click "Push", and check the box 
    next to the feature you were working on.
    SourceTree will create a branch on the remote
    system (aka origin) with the same name
    as your current branch.

7.  At this stage you can review your continuous integration
    system. Your changes should be building and will be tested
    automatically.

Making sure your changes still work with latest master
---------------------------------------------------------

1.  Now, let us say you have made a series of
    commits and your tests have passed and you
    would like to get your changes accepted into
    `master`. 

2.  Firstly, make sure your master is up to date by 
    double clicking on `master`. If you get an error message
    see following step.

3.  If there are changes that you haven't committed, switching 
    to a different branch can cause the loss of your unchanged
    files. Git prevents you from doing this. You can stash your
    work in progress away by clicking "Stash" on the toolbar.
    I suggest you use a description that matches your branch name.
    For example, `WIP 122 Plot Thickens` (for Work in Progress).
    You should see a "Stashes" folder appear on the sidebar 
    on SourceTree. Now you should be able to safely check out
    the master branch by double clicking on "master".

4.  At this stage, your working directory will be identical
    to the files in the master branch. However, it is quite
    likely your master branch is out of date. There's probably
    changes to the origin/master branch that you need to pull
    in. Click on "Pull" to pull in the changes.

5.  Now, how do we know whether your changes will work
    with the latest master? There are two approaches. First
    is to replay all your changes on top of the latest master 
    (this is known as a `rebase` operation). Alternately
    you can `merge` the changes from master into you branch.
    We will be doing the former.

6.  Rebase operation : let's check out your previous branch
    by double clicking on it again. Then select "master",
    right-click and select "Rebase current changes onto master".
    You should confirm you intend to Rebase.

7.  Next you will need to push your rebased branch back to
    origin. This will have to be pushed by "force", effectively
    deleting the feature branch on the master and creating a new
    feature with the same name. Click on `Terminal`. Type 
    `git branch` to verify you are currently in your feature
    branch. Then type `git push --force`. Next close the window.
    
Making a pull request
---------------------

1. In your web browser, go to your Stash server. On the left 
   panel, click on "Create pull request".

2. In the top drop down, select your feature branch, and 
   in the bottom drop down, where the arrow leads to 
   so be "master"

3. Click on "Continue"


Checking whether a pull request is valid
-------------------------------------------

1.  This section is only relevant to the maintainer of 
    the "master" branch.
   
2.  Make sure the feature branch builds and unit tests 
    have passed.
   
3.  Secondly, we should check that the feature has been
    rebased on master. We will do this in the terminal. 
    Click on the terminal icon.
   
4.  Switch to master branch and make sure you are up-to-date.
   
            git checkout master
            git pull
   
   
5.  Create a temporary branch
   
            git checkout -b temp/122
            git merge --ff-only origin/feature/122-plot-thickens
   
6.  If the feature branch was rebased on an out-of-date
    master, then the previous step will fail with the following
    message.
   
    ```fatal: Not possible to fast-forward, aborting.```

7.  Clean up by deleting the temporary branch

            git checkout master
            git branch -d temp/122



    



    




