﻿using NUnit.Framework;
using System.IO;
using System.Linq;

namespace Gadsby.Test
{
    public class Lipogram
    {
        [TestCase("../../../src/Chapter1.txt")]
        public void File_Must_Not_Contain_Letter_E(string fileName)
        {
            var lines = File.ReadAllLines(fileName);
            var badLines = lines
                .Select((text, lineNumber) =>
                {
                    return new
                    {
                        Contains_E = (text.ToLower().Contains("e")),
                        LineNumber = lineNumber + 1,
                        Text = text,
                    };
                })
                .Where(x => x.Contains_E)
                .Where(x => !x.Text.Contains("Wright, Ernest Vincent"))
                .ToList();

            if (badLines.Any())
            {
                var errorMessage = string.Join("\n", badLines.Select(x => $"Line {x.LineNumber:###0} : {x.Text} "));
                Assert.Fail(errorMessage);
            }
        }
    }
}
