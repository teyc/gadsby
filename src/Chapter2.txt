By now, Branton Hills 
was so proud of not only its "smarting up," but also 
of its startling growth, on that account, that an ap- 
plication was put forth for its incorporation as a 
city; a small city, naturally, but full of that condi- 
tion of Youth, known as "growing pains." So its 
shabby old "Town Hall" sign was thrown away, and 
a black and gold onyx slab, with "City Hall" 
blazing forth in vivid colors, put up, amidst band 
music, flag waving, parading and oratory. In only 
a month from that glorious day, Gadsby found folks 
"primping up": girls putting on bright ribbons; 
boys finding that suits could stand a good ironing; 
and rich widows and portly matrons almost out- 
doing any rainbow in brilliancy. An occasional shop 
along Broadway, which had a rattly door or shaky 
windows was put into first class condition, to fit 
Branton Hills' status as a city. Old Bill Simpkins 
was strutting around, as pompous as a drum-major ; 
for, now, that old Town Council would function as 
a CITY council; HIS council! His own stamping 
ground ! According to him, from it, at no far day, 
"Bill Simpkins, City Councilman," would show an 
anxiously waiting world how to run a city ; though 
probably, I think, how not to run it. 

It is truly surprising what a narrow mind, 
what a blind outlook a man, brought up with prac- 
tically no opposition to his boyhood wants, can at- 
tain ; though brought into contact with indisputably 
important data for improving his city. Our Organ- 
ization boys thought Bill "a bit off;" but Gadsby 
would only laugh at his blasts against paying out 
city funds; for, you know, all bombs don't burst; 
you occasionally find a "dud." 

But this furor for fixing up rattly doors or 
shaky windows didn't last; for Old Bill's oratory 
found favor with a bunch of his old tight-wads, who 
actually thought of inaugurating a campaign 
against Gadsby's Organization of Youth. As soon 
as this was known about town, that mythical pot, 
known as Public Opinion, was boiling furiously. A 
vast majority stood back of Gadsby and his kids; 
so, old Bill's ranks could count only on a small group 
of rich old Shylocks to whom a bank-book was a 
thing to look into or talk about only annually ; that 
is, on bank-balancing days. This small minority 
got up a slogan : � "Why Spoil a Good Old Town ?" 
and actually did, off and on, talk a shopman out of 
fixing up his shop or grounds. This, you know, put 
additional vigor into our Organization ; inspiring a 
boy to bring up a plan for calling a month, � say 
July, � "pick-up, paint-up and wash-up month ;" for 
it was a plain fact that, all about town, was many a 
shabby spot; a lot of buildings could stand a good 
coat of paint, and yards raking up; thus showing 
surrounding towns that not only could Branton 
Hills "doll up," but had a class of inhabitants who 
gladly would go at such a plan, and carry it through. 
So Gadsby got his "gang" out, to sally forth and 
any man or woman who did not jump, at first, at 
such a plan by vigorous Youth, was always brought 
around, through noticing how poorly a shabby yard 
did look. So Gadsby put in Branton Hills' "Post" 
this stirring call : � 

"Raking up your yard or painting your build- 
ing is simply improving it both in worth; ar "stically 
and from a utilization standpoint. I know that 
many a city front lawn is small ; but, if it is only 
fairly big, a walk, cut curvingly, will add to it, sur- 
prisingly. That part of a walk which runs to your 
front door could show rows of small rocks rough 
and natural; and grading from small to big; but 
no 'hit-or-miss' layout. You can so fix up your yard 
as to form an approach to unity in plan with such 
as adjoin you; though without actual duplication; 
thus providing harmony for all who may pass by. 

It is, in fact, but a bit of City Planning; and any- 
body who aids in such work is a most worthy inhab- 
itant. So, cut your scraggly lawns! Trim your 
old, shaggy shrubs ! Bring into artistic form, your 
grass-grown walks!" 

(Now, naturally, in writing such a story as this, 
with its conditions as laid down in its Introduction, 
it is not surprising that an occasional "rough spot" 
in composition is found. So I trust that a critical 
public will hold constantly in mind that I am volun- 
tarily avoiding words containing that symbol which 
is, by far, of most common inclusion in writing 
our Anglo-Saxon as it is, today. Many of our most 
common words cannot show; so I must adopt syno- 
nyms; a*-d so twist a thought around as to say what 
I wish'' ith as much clarity as I can.) 

So, now to go on with this odd contraption : 

By Autumn, a man who took his vacation in 

July, would hardly know his town upon coming back , 

so thoroughly had thousands "dug in" to aid in its 

transformation. 

"Boys," said Gadsby, "you can pat your own 
backs, if you can't find anybody to do it for you. 
This city is proud of you. And, girls, just sing 
with joy ; for not only is your city proud of you, but 
I am. too." 

"But how about you, sir, and your work?" 

This was from Frank; a boy brought up to 
think fairly on all things. "Oh," said Gadsby laugh- 
ingly, "I didn't do much of anything but boss you 
young folks around. If our Council awards any 
diplomas, I don't want any. I would look ridicu- 
lous strutting around with a diploma with a pink 
ribbon on it, now wouldn't I !" 

This talk of diplomas was as a bolt from a 
bright sky to this young, hustling bunch. But, 
though Gadsby's words did sound as though a 
grown man wouldn't want such a thing, that wasn't 
saying that a young boy or girl wouldn't ; and with 
this surprising possibility ranking in young minds, 
many a kid was in an anti-soporific condition for 
parts of many a night. 

But a kindly Councilman actually did bring 
up a bill about this diploma affair, and his 
collaborators put it through; which naturally 
brought up talk as how to award such diplomas. 
At last it was thought that a big public affair at 
City Hall, with our Organization on a platform, 
with Branton Hills' Mayor and Council, would 
furnish an all-round, satisfactory way. 

Such an occasion was worthy of a lot of 
planning; and a first thought was for flags and 


bunting on all public buildings ; with a grand illum- 
ination at night. Stationary lights should glow from 
all points on which a light could stand, hang, or 
swing; and gigantic rays should swoop and swish 
across clouds and sky. Bands should play ; boys and 
girls march and sing; and a vast crowd would pour 
into City Hall. As on similar occasions, a bad rush 
for chairs was apt to occur, a company of military 
units should occupy all important points, to hold 
back anything simulating a jam. 

Now, if you think our Organization wasn't 
all agog and wild, with youthful anticipation at hav- 
ing a diploma for work out of school hours, you 
just don't know Youth. Boys and girls, though not 
full grown inhabitants of a city, do know what will 
add to its popularity; and having had a part in 
bringing about such conditions, it was but natural 
to look back upon such, as any military man might 
at winning a difficult fight. 

So, finally our big day was at hand! That 
it might not cut into school hours, it was on a Satur- 
day; and, by noon, about a thousand kids, singing, 
shouting and waving flags, stood in formation at 
City Park, awaiting, with growing thrills, a signal 
which would start as big a turn-out as Branton Hills 
had known in all its history. Up at City Hall 


awaiting arrivals of city officials, a big crowd sat; 
row upon row of chairs which not only took up all 
floor room, but also many a small spot, in door-way 
or on a balcony in which a chair or stool could find 
footing; and all who could not find such an oppor- 
tunity willingly stood in back. Just as a group of 
officials sat down on that flag-bound platform, dis- 
tant throbbing of drums, and bright, snappy band 
music told of Branton Hills' approaching thousands 
of kids, who, finally marching in through City Hall's 
main door, stood in a solid mass around that big 
room. 

Naturally Gadsby had to put his satisfaction 
into words; and, advancing to a mahogany stand, 
stood waiting for a storm of hand-clapping and 
shouts to quit, and said : � 

"Your Honor, Mayor of Branton Hills, its 
Council, and all you out in front: � If you would 
only stop rating a child's ability by your own; and 
try to find out just what ability a child has, our 
young folks throughout this big world would show a 
surprisingly willing disposition to try things which 
would bring your approbation. A child's brain is 
an astonishing thing. It has, in its construction, an J 
astounding capacity for absorbing what is brought ( 
to it ; and not only to think about, but to find ways I
for improving it. It is today's child who, tomorrow, 
will, you know, laugh at our ways of doing things. 
So, in putting across this campaign of building up 
our community into a municipality which has won 
acclaim, not only from its officials and inhabitants, 
but from surrounding towns I found, in our young 
folks, an out-and-out inclination to assist ; and you, 
today, can look upon it as labor in which your adult 
aid was but a small factor. So now, my Organiza- 
tion of Youth, if you will pass across this platform, 
your Mayor will hand you your diplomas." 
